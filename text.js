var isTexting;

var font = '14px sans-serif';
var hasInput = false;
var font_name = 'Arial';
var font_size=2;


      
function text(){

    hasInput=false;
    canvas.onclick = function(e) {
        if (hasInput) return;
        addInput(e.offsetX, e.offsetY);
    
    }
}
      
            
function addInput(x, y) {
  
    var input = document.createElement('input');
    
    font = 8*font_size+'px '+font_name;
    input.type = 'text';
    input.style.position = 'static';
    input.style.left = (x - 4) + 'px';
    input.style.top = (y - 4) + 'px';
    input.style.font = font;
    input.style.color = currentColor;

    input.onkeydown = handleEnter;

    document.body.appendChild(input);
   
    input.focus();
    
    hasInput = true;
}

function handleEnter(e) {
    var keyCode = e.keyCode;
    
    if (keyCode === 13) {
        drawText(this.value, parseInt(this.style.left, 10), parseInt(this.style.top, 10));
        document.body.removeChild(this);
        hasInput = false;
    }
}

function drawText(txt, x, y) {
    context.fillStyle = currentColor;
    context.textBaseline = 'top';
    context.textAlign = 'left';
    context.font = font;
    context.fillText(txt, x-4, y-4);
    
    cPush();
}

function setFont(f) {
  if (f == 0) {
    font_name = 'Arial';
  } else if (f == 1) {
    font_name = 'Verdana';
  } else if (f == 2) {
    font_name = 'Courier New';
  } else if (f == 3) {
    font_name = 'serif';
  } else if (f == 4) {
    font_name = 'sans-serif';
  } else if (f == 5) {
    font_name = '微軟正黑體';
  } else if (f == 6) {
    font_name = '新細明體';
  } else if (f == 7) {
    font_name = '標楷體';
  }
}

function setFont_size(value) {
  font_size = Math.round(parseInt(value) / 10);
  if (font_size == 0)
    font_size = 1;
}
