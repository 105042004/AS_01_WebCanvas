var colors = ['black', 'grey', 'red', 'orange', 'yellow','green', 'blue', 'violet'];
var currentColor;


for(var i=0, n=colors.length;i<n;i++){
    var swatch = document.createElement('div');
    swatch.className = 'swatch';
    swatch.style.backgroundColor = colors[i];
    swatch.addEventListener('click', setSwatch);
    document.getElementById('colors').appendChild(swatch);
}

function setColor(color){
    context.fillStyle = color;
    context.strokeStyle = color;
    currentColor = color;
    var active = document.getElementsByClassName('active')[0];
    if(active){
        active.className = 'swatch';
    }
}

function setSwatch(e){
    var swatch = e.target; //identify swatch
    setColor(swatch.style.backgroundColor); //set color
    swatch.className += ' active'//give active class

}

setSwatch({target: document.getElementsByClassName('swatch')[0]});