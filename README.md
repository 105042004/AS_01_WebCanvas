# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

|                       **Item**                   | **Score** |
|:--------------------------------------------:|:-----:|
|               Basic components               |  60%  |
|                 Advance tools                |  35%  |
|            Appearance (subjective)           |   5%  |
| Other useful widgets (**describe on README.md**) | 1~10% |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

## Function Details
1. toolbar右側，運用簡單CSS的3D效果做出精美的顏色選擇器，會根據使用中的顏色改變外框
2. eraser運用'destination-out'屬性製作，並非塗白
3. 輸出無預設背景的png透明背景圖檔
4. 文字輸入可選擇字體、粗細
5. 圖片輸入可以選擇圖片後再由數標決定放置位置
6. Width按鈕調整pen及eraser粗細
7. 選擇欲上傳的檔案後再按佑側upload確認上傳
8. pen, eraser, text工具，會依據使用中的改變游標造型
9. refresh清除畫布
10. download下載圖片

