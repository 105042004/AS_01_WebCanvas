var setRadius = function(newRadius){
    if(newRadius<minRad)
        newRadius = minRad;
    else if(newRadius>maxRad)
        newRadius = maxRad;
        radius = newRadius;
        context.lineWidth = radius*2;

        radSpan.innerHTML = radius;
}



var minRad = 1,
    maxRad = 100,   
    defaultRad = 5,
    interval = 1;
    radSpan = document.getElementById('radval'),
    decrad = document.getElementById('decrad'),
    incrad = document.getElementById('incrad');

decrad.addEventListener('click', function(){
    setRadius(radius-interval);
})

incrad.addEventListener('click', function(){
    setRadius(radius+interval);
})

setRadius(defaultRad);