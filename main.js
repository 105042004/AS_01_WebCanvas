var canvas = document.getElementById('canvas');
var context = canvas.getContext('2d');
var clear = document.getElementById('clear');
var imgToDraw;
var tempOnmouseup;
var tempOnmousemove;
var tempOnmousedown;
var upload = false;

var radius = 10;
var dragging = false;
var brushmode= "pen";
var transparent = '#ff0000';

document.body.style.cursor="url('./images/pen.png'), auto";

canvas.width = window.innerWidth;
canvas.height = window.innerHeight;

window.onresize = function(){
    var image = context.getImageData(0, 0, canvas.width, canvas.height);
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;
    context.putImageData(image, 0, 0);
}

var clearCanvas = function(canvas){
    canvas.width = canvas.width;
    context.lineWidth = radius*2;
}

context.lineWidth = radius*2;

var putPoint = function(e){
    if((dragging)&&(brushmode!="text")){

        context.lineTo(e.offsetX, e.offsetY);
        context.stroke();
        context.beginPath();
        context.arc(e.offsetX, e.offsetY, radius, 0, Math.PI*2);
        context.fill();
        context.beginPath();
        context.moveTo(e.offsetX, e.offsetY);

        while(upload){
            var x = e.clientX - e.target.offsetLeft;
            var y = e.clientY - e.target.offsetTop;
            context.drawImage(imgToDraw, x, y);
            brushmode = "pen";
            brush();
            upload = false;
        }
    
    }
}

function uploadImage(e) {
    var reader = new FileReader();
      reader.onload = function(event){
          imgToDraw = new Image();
          imgToDraw.src = event.target.result;
          console.log(imgToDraw);
      }
      reader.readAsDataURL(e.target.files[0]); 
  
  }


var  engage = function(e){
    dragging = true;
    putPoint(e);

}

var  disengage = function(){
    dragging = false;
    context.beginPath();
    
}


function brush(){
    if(brushmode=="pen"){
        context.globalCompositeOperation='source-over';
        document.body.style.cursor="url('./images/pen.png'), auto";
    }
    else if(brushmode=="eraser"){
        context.globalCompositeOperation='destination-out';
        document.body.style.cursor="url('./images/eraser.png'), auto";
        
    }
    else if(brushmode=="text"){
        //context.globalCompositeOperation='source-over';
        document.body.style.cursor="url('./images/text.png'), auto";
        text();
    }
    else if(brushmode=="upload"){
        document.body.style.cursor="url('./images/upload.png'), auto";
    }
    
}

function setPen(){
    brushmode = "pen";
    brush();
}


function setErase(){
    brushmode = "eraser";
    brush();
}

function setText(){
    brushmode = "text";
    brush();
}

function startToDrawImage() {
    upload = true;
    brushmode = "upload";
    brush();
  }



canvas.addEventListener('mousedown', engage);
canvas.addEventListener('mousemove', putPoint);
canvas.addEventListener('mouseup', disengage);

clear.addEventListener('click', function(){
    clearCanvas(canvas);
})

document.getElementById("download").addEventListener("click",function (){
    var link = document.getElementById("download");
    link.href = 'canvas'.toDataURL();
    link.download = "image.png";
})
